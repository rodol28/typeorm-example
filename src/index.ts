import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import userRoutes from './routes/users.routes';
import { createConnection } from 'typeorm';
import 'reflect-metadata';

const app = express();
createConnection();

// middlewares
app.use(morgan('dev'));
app.use(cors());
app.use(express.json());


// routes
app.use(userRoutes);

app.listen(3000, () => {
    console.log('server on port 3000');
});